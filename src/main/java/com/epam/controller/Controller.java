package com.epam.controller;

import com.epam.model.task1.PingPong;
import com.epam.model.task2.Fibonacci;
import com.epam.model.task2.Fibonacci2;
import com.epam.model.task2.Fibonacci3;
import com.epam.model.task4.FibonacciWithCallable;
import com.epam.model.task5.Task5;
import com.epam.model.task6.Task6Impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Controller {
    PingPong pingPong = new PingPong();
    Task5 task5 = new Task5();
    FibonacciWithCallable fibonacciWithCallable = new FibonacciWithCallable(5);
    Task6Impl task6 = new Task6Impl();

    public void playPingPong() {
        System.out.println(pingPong.play());
    }

    public void getFibonacciNumbers() {
        Thread f1 = new Thread(new Fibonacci(15));
        Thread f2 = new Thread(new Fibonacci2(15));
        Thread f3 = new Thread(new Fibonacci3(15));
        f1.start();
        f2.start();
        f3.start();
    }

    public void getFibonacciNumbersWithExecutor() {
        System.out.println("\nFibonacci numbers using CachedThreadPool\n");
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new Fibonacci(15));
        executorService.execute(new Fibonacci2(15));
        executorService.execute(new Fibonacci3(15));
        executorService.shutdown();

        System.out.println("\nFibonacci numbers using FixedThreadPool\n");
        ExecutorService executorService1 = Executors.newFixedThreadPool(4);
        executorService1.execute(new Fibonacci(15));
        executorService1.execute(new Fibonacci2(15));
        executorService1.execute(new Fibonacci3(15));
        executorService1.shutdown();

        System.out.println("\nFibonacci numbers using SingleThreadExecutor\n");
        ExecutorService executorService2 = Executors.newSingleThreadExecutor();
        executorService2.execute(new Fibonacci(15));
        executorService2.execute(new Fibonacci2(15));
        executorService2.execute(new Fibonacci3(15));
        executorService2.shutdown();
    }

    public void executeTask4() {
        fibonacciWithCallable.executeTask4();
    }

    public void executeTask5() {
        task5.create();
    }
    public void executeTask6() throws InterruptedException {
        task6.startSameObjectSync();
    }

}
