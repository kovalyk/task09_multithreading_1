package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    public final static String EXIT = "Q";
    public Logger logger = LogManager.getLogger(View.class);
    private Controller controller = new Controller();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;

    public View() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Show Ping Pong game");
        menu.put("2", "\t2 - Show Fibonacci numbers");
        menu.put("3", "\t3 - Show Fibonacci numbers using different Executors");
        menu.put("4", "\t4 - Show Fibonacci numbers using Callable");
        menu.put("5", "\t5 - Show result for ScheduledThreadPool");
        menu.put("6", "\t6 - Show synchronization of the three methods");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showPingPong);
        methodsMenu.put("2", this::showFibonacci);
        methodsMenu.put("3", this::showFibonacciThroughExecutor);
        methodsMenu.put("4", this::showFibonacciUsingCallable);
        methodsMenu.put("5", this::showTask5);
        methodsMenu.put("6", this::showTask6);
    }

    private void showPingPong() {
        controller.playPingPong();

    }

    private void showFibonacci() {
        controller.getFibonacciNumbers();
    }

    private void showFibonacciThroughExecutor() {
        controller.getFibonacciNumbersWithExecutor();
    }

    private void showFibonacciUsingCallable() {
        controller.executeTask4();
    }

    private void showTask5() {
        controller.executeTask5();
    }

    private void showTask6() throws InterruptedException {
        controller.executeTask6();
    }

    private void outputMenu() {
        logger.warn("\nMENU:");
        for (String str : menu.values()) {
            logger.warn(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.warn("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.error("Invalid input! Please try again.");
            }
        } while (!keyMenu.equals(EXIT));
    }
}
