package com.epam.view;

import java.io.IOException;

public interface Printable {
    void print() throws InterruptedException;
}
