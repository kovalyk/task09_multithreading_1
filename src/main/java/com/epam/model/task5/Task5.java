package com.epam.model.task5;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Create a task that sleeps for a random amount of time between 1 and 10 seconds,
 * then displays its sleep time and exits. Create and run a quantity (given on the command
 * line) of these tasks. Do it by using ScheduledThreadPool.
 */

public class Task5 {
    Random rand = new Random();

    public void create() {
        Random random = new Random();
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(3);
        System.out.println("Please, enter number of tasks:");
        Scanner scanner = new Scanner(System.in);
        int numberOfTasks = scanner.nextInt();
        for (int i = 0; i < numberOfTasks; i++) {
            final int sleep = random.nextInt(10) + 1;
            executor.schedule(
                    () -> System.out.println(Thread.currentThread().getName() + " slept "
                            + sleep + " seconds"), sleep, TimeUnit.SECONDS);
        }
        executor.shutdown();
    }
}
