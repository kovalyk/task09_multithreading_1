package com.epam.model.task6;

public class Task6Impl {

    public void startSameObjectSync() throws InterruptedException {
        System.out.println("Synchronize on the same object");
        Task6 sameObjectSync = new Task6();
        startAndJoinThread(new Thread(sameObjectSync::method1),
                new Thread(() -> sameObjectSync.method2()),
                new Thread(() -> sameObjectSync.method3()));
    }

    private void startAndJoinThread(Thread thread1, Thread thread2, Thread thread3)
            throws InterruptedException {
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();
    }
}
