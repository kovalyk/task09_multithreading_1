package com.epam.model.task6;

/**
 * Create a class with three methods containing critical sections that all synchronize on
 * the same object. Create multiple tasks to demonstrate that only one of these methods
 * can run at a time. Now modify the methods so that each one synchronizes on a different
 * object and show that all three methods can be running at once.
 */

public class Task6 {
    private final Object object = new Object();

    public void method1() {
        synchronized (object) {
            System.out.println(Thread.currentThread().getName() + " method1 is running");
            System.out.println(Thread.currentThread().getName() + " method1 finished");
        }
    }

    public void method2() {
        synchronized (object) {
            System.out.println(Thread.currentThread().getName() + " method2 is running");
            System.out.println(Thread.currentThread().getName() + " method2 finished");
        }
    }

    public void method3() {
        synchronized (object) {
            System.out.println(Thread.currentThread().getName() + " method3 is running");
            System.out.println(Thread.currentThread().getName() + " method3 finished");
        }
    }
}
