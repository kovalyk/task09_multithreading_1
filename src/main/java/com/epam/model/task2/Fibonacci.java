package com.epam.model.task2;

import com.epam.view.View;

public class Fibonacci implements Runnable {
    public static View view;
    private int n = 0;

    public Fibonacci(int n) {
        this.n = n;
    }

    private int getNumber(int x) {
        if (x < 2) return 1;
        return getNumber(x - 2) + getNumber(x - 1);
    }

    public void run() {
        for (int i = 0; i < n; i++)
            System.out.println(getNumber(i) + " ");
    }
}
