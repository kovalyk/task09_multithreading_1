package com.epam.model.task1;

public class PingPong {
    private volatile static StringBuilder pingPongResult = new StringBuilder();
    private static Object sync = new Object();
    private static final int NUMBER_OF_TIMES = 10;

    private void getPing() {
        synchronized (sync) {
            for (int i = 0; i < NUMBER_OF_TIMES; i++) {
                sync.notify();
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pingPongResult.append("Ping\n");
                sync.notify();
            }
        }
    }

    private void getPong() {
        synchronized (sync) {
            for (int i = 0; i < NUMBER_OF_TIMES; i++) {
                sync.notify();
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pingPongResult.append("Pong\n");
                sync.notify();
            }
        }
    }

    public String play() {
        pingPongResult = new StringBuilder();
        Thread t1 = new Thread(() -> getPing());
        Thread t2 = new Thread(() -> getPong());
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return pingPongResult.toString();
    }
}