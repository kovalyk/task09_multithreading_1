package com.epam.model.task3;

public class Fibonacci3 implements Runnable {
    private int n = 0;

    public Fibonacci3(int n) {
        this.n = n;
    }

    private int getNumber(int x) {
        if (x < 2) {
            return 1;
        } else {
            return getNumber(x - 2) + getNumber(x - 1);
        }
    }

    public void run() {
        for (int i = 0; i < n; i++)
            System.out.println(getNumber(i) + " ");
    }
}
