package com.epam.model.task4;

import java.util.ArrayList;
import java.util.concurrent.*;

public class FibonacciWithCallable implements Callable<Integer> {
    private int n = 0;
    public FibonacciWithCallable(int n) {
        this.n = n;
    }
    private int getNumber(int x) {
        if(x < 2) return 1;
        return getNumber(x - 2) + getNumber(x - 1);
    }
    public Integer call() {
        int result = 0;
        for(int i = 0; i < n; i++)
            result += getNumber(i);
        return result;
    }
    public void executeTask4() {
        ExecutorService exec = Executors.newCachedThreadPool();
        ArrayList<Future<Integer>> results = new ArrayList<Future<Integer>>();
        for (int i = 0; i < 20; i++)
            results.add(exec.submit(new FibonacciWithCallable(i)));
        for (Future<Integer> fs : results)
            try {
                System.out.println(fs.get());
            } catch (InterruptedException e) {
                System.out.println(e);
                return;
            } catch (ExecutionException e) {
                System.out.println(e);
            } finally {
                exec.shutdown();
            }
    }
}
